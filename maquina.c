#include <stdio.h>

typedef enum{
    goNORTE,
    waitNORTE,
	goESTE,
	waitESTE
}Estados;

typedef enum{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
}Entradas;


static int EstadoActual = goNORTE;

void impEstadoActual(int eActual){
	
        if(eActual ==goNORTE){
            printf("goNORTE");
            }
        if(eActual == waitNORTE){
            printf("waitNORTE");
            }
        if(eActual == goESTE){
            printf("goESTE");
            }
        if(eActual == waitESTE){
            printf("waitESTE");
            }
	
};



int maquina(int entrada, int estado)
{

	if (estado == goNORTE){
            if (entrada == NO_CARROS || entrada == CARROS_NORTE){
		        EstadoActual = goNORTE;
            }else{
		        EstadoActual = waitNORTE;
            }
        }
    
    if (estado == waitNORTE){
	        EstadoActual = goESTE;
        }

    if (estado == goESTE){
            if (entrada == NO_CARROS || entrada == CARROS_ESTE){
		        EstadoActual = goESTE;
            }else{
		        EstadoActual = waitESTE;
            }
        }
    
    if (estado == waitESTE){
	        EstadoActual = goNORTE;
        
    }
		
	 impEstadoActual(EstadoActual);
};


int main(){
    int Transiciones[8] ={
        CARROS_ESTE, 
	NO_CARROS,
        CARROS_ESTE, 
        CARROS_NORTE, 
        AMBOS_LADOS,
	CARROS_NORTE,
        NO_CARROS,
        AMBOS_LADOS
    };	

    printf("\nInicio [");
    impEstadoActual(EstadoActual);
    printf("]");    

    for (int i = 0; i < 8; i++){
	    printf("\nEstado Actual [");
        maquina(Transiciones[i], EstadoActual);
        printf("]");
    }
    printf("\n\n");
    return 0;    
}
