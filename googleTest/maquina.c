#include <stdio.h>

typedef enum{
    goNORTE,
    waitNORTE,
	goESTE,
	waitESTE
}Estados;

typedef enum{
	NO_CARROS,
	CARROS_ESTE,
	CARROS_NORTE,
	AMBOS_LADOS
}Entradas;


static int EstadoActual = goNORTE;



int maquina(int entrada, int estado)
{

	if (estado == goNORTE){
            if (entrada == NO_CARROS || entrada == CARROS_NORTE){
		        EstadoActual = goNORTE;
            }else{
		        EstadoActual = waitNORTE;
            }
        }
    
    if (estado == waitNORTE){
	        EstadoActual = goESTE;
        }

    if (estado == goESTE){
            if (entrada == NO_CARROS || entrada == CARROS_ESTE){
		        EstadoActual = goESTE;
            }else{
		        EstadoActual = waitESTE;
            }
        }
    
    if (estado == waitESTE){
	        EstadoActual = goNORTE;
        
    }
		
	 return  EstadoActual;
};


